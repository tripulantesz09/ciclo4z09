package com.example.ciclo4demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InicioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        TextView usuario = findViewById(R.id.inicio_tvwUsuario);

        Bundle datos = getIntent().getExtras();
        if(datos != null)
            usuario.setText(datos.getString("usuario"));

        Button btnAgregar = findViewById(R.id.inicio_btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AgregarUsuarioActivity.class);
                startActivity(intent);
            }
        });

        Button btnBuscar = findViewById(R.id.inicio_btnConsultar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListarUsuariosListViewActivity.class);
                startActivity(intent);
            }
        });

        Button btnBuscarRecycler = findViewById(R.id.inicio_btnConsultarRecycler);
        btnBuscarRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListarUsuariosRecyclerActivity.class);
                startActivity(intent);
            }
        });

    }
}