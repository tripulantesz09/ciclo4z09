package com.example.ciclo4demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.ciclo4demo.adapters.UsuarioArrayAdapter;
import com.example.ciclo4demo.dao.UsuarioDAO;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;

public class ListarUsuariosListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_usuarios_list_view);

        UsuarioDAO baseDatos = new UsuarioDAO(this);
        ArrayList<Usuario> resultadoObtenido = baseDatos.listar();

        UsuarioArrayAdapter adaptadorUsuario = new UsuarioArrayAdapter(ListarUsuariosListViewActivity.this, R.layout.listview_itemusuario, resultadoObtenido);

        ListView lista = findViewById(R.id.listarUsuariosListView_lista);
        lista.setAdapter(adaptadorUsuario);


    }
}