package com.example.ciclo4demo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ciclo4demo.classes.Conexion;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;

public class UsuarioDAO {
    private Context contexto;
    private Conexion conexion;
    private final String nombreTabla = "usuarios";

    public UsuarioDAO(Context contexto) {
        this.contexto = contexto;
        conexion = new Conexion(this.contexto);
    }
    public long insertar(Usuario usuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();
        ContentValues valoresAInsertar = new ContentValues();
        valoresAInsertar.put("tipdoc", usuario.getTipdoc());
        valoresAInsertar.put("documento", usuario.getDocumento());
        valoresAInsertar.put("nombres", usuario.getNombres());
        valoresAInsertar.put("apellidos", usuario.getApellidos());
        valoresAInsertar.put("direccion", usuario.getDireccion());
        valoresAInsertar.put("email", usuario.getEmail());
        valoresAInsertar.put("clave", usuario.getClave());

        return baseDatos.insert(this.nombreTabla, null, valoresAInsertar);
    }

    public ArrayList<Usuario> listar()
    {
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

        SQLiteDatabase baseDatos = this.conexion.getReadableDatabase();

        String[] columnasConsultar = { "id", "tipdoc", "documento", "nombres", "apellidos", "direccion","email", "clave" };

        Cursor cursor = baseDatos.query(this.nombreTabla, columnasConsultar, null, null, null, null, null);

        if(cursor!=null)
        {
            cursor.moveToFirst();

            do {
                Usuario registro = new Usuario(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7)
                );
                usuarios.add(registro);
            } while(cursor.moveToNext());

        }

        return usuarios;
    }

    public Usuario listarPorDocumento(String documento)
    {
        Usuario usuario = null;

        SQLiteDatabase baseDatos = this.conexion.getReadableDatabase();

        String[] columnasConsultar = { "id", "tipdoc", "documento", "nombres", "apellidos", "direccion","email", "clave" };

        String condicionWhere = "documento = ?";
        String[] valorCondicionWhere = { documento };

        Cursor cursor = baseDatos.query(this.nombreTabla, columnasConsultar, condicionWhere, valorCondicionWhere, null, null, null);

        if(cursor!=null)
        {
            cursor.moveToFirst();

            do {
                usuario = new Usuario(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7)
                );
            } while(cursor.moveToNext());

        }

        return usuario;
    }

    public int guardarCambios(Usuario usuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();
        ContentValues valoresAActualizar = new ContentValues();
        valoresAActualizar.put("tipdoc", usuario.getTipdoc());
        valoresAActualizar.put("documento", usuario.getDocumento());
        valoresAActualizar.put("nombres", usuario.getNombres());
        valoresAActualizar.put("apellidos", usuario.getApellidos());
        valoresAActualizar.put("direccion", usuario.getDireccion());
        valoresAActualizar.put("email", usuario.getEmail());
        valoresAActualizar.put("clave", usuario.getClave());

        String criterioWhere = "id = ?";
        String[] argumentoCriterioWhere = { String.valueOf(usuario.getId()) };

        return baseDatos.update(this.nombreTabla,valoresAActualizar,criterioWhere,argumentoCriterioWhere);
    }

    public int eliminar(Usuario usuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();
        String criterioWhere = "id = ?";
        String[] argumentoCriterioWhere = { String.valueOf(usuario.getId()) };
        return baseDatos.delete(this.nombreTabla,criterioWhere,argumentoCriterioWhere);
    }
}
