package com.example.ciclo4demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;

import com.example.ciclo4demo.dao.UsuarioDAO;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;

public class ListarUsuariosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listarusuarios);

        ImageButton btnEditar = (ImageButton) findViewById(R.id.listarUsuario_btnEditar);
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Presionaste el boton editar");
            }
        });

        ImageButton btnEliminar = (ImageButton) findViewById(R.id.listarUsuarios_btnEliminar);
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Presionaste el boton eliminar");
            }
        });

        RatingBar puntuacion = findViewById(R.id.ratingBar);
        puntuacion.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                System.out.println(rating);
            }
        });

        //Establecer la conexion a la base de datos
        UsuarioDAO baseDatos = new UsuarioDAO(this);
        //Consultamos todos los registros de la base de datos y los almacenamos en una
        //lista de tipo Usuario
        ArrayList<Usuario> resultadoConsulta = baseDatos.listar();

        //Declaramos la Lista de tipo String usuarios
        ArrayList<String> usuarios = new ArrayList<String>();

        //Llenamos la lista usuarios concatenando el documento, nombres y apellidos
        for(Usuario registro : resultadoConsulta)
            usuarios.add(registro.getDocumento() + " " + registro.getNombres() + " " + registro.getApellidos());

        //Establecemos un adaptador indicandole el layout a copiar y los datos que tendra cada uno de estos.
        ArrayAdapter<String> adaptadorUsuarios = new ArrayAdapter<String>(this, android.R.layout.select_dialog_multichoice, usuarios);

        //Conectamos el componente del layout "activity_listarusuarios.xml" a la variable de tipo ListView llamado lista
        ListView lista = findViewById(R.id.listarUsuarios_lista);

        //Establecemos el adaptador al listview
        lista.setAdapter(adaptadorUsuarios);

    }
}