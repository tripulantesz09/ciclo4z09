package com.example.ciclo4demo.modelos;

import junit.framework.TestCase;

public class UsuarioTest extends TestCase {

    public void testTestToString() {
        Usuario modelo = new Usuario("CC", "1098643763", "Mario", "Montoya", "Carrera 20", "mmontoya@gmail.com", "mmontoya");

        String info = "/*** Información del Usuario ***/\n";
        info+="Documento: (CC) 1098643763\n";
        info+="Nombre Completo: Mario Montoya\n";
        info+="Direccion: Carrera 20\n";
        info+="Email: mmontoya@gmail.com\n";

        assertEquals(info, modelo.toString());

    }

    public void testSetNombres() {
        Usuario modelo = new Usuario();
        modelo.setNombres("Diego");
        String resultadoObtenido = modelo.getNombres();
        String resultadoEsperado = "DIEGO";
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    public void testSetApellidos() {
        Usuario modelo = new Usuario();
        modelo.setApellidos("VEGA");
        String resultadoObtenido = modelo.getApellidos();
        String resultadoEsperado = "VEGA";
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}