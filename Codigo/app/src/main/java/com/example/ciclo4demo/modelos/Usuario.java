package com.example.ciclo4demo.modelos;

import java.util.Locale;

public class Usuario {

    private int id;
    private String tipdoc;
    private String documento;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String email;
    private String clave;

    public Usuario() {
    }

    public Usuario(String tipdoc, String documento, String nombres, String apellidos, String direccion, String email, String clave) {
        this.tipdoc = tipdoc;
        this.documento = documento;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.email = email;
        this.clave = clave;
    }

    public Usuario(int id, String tipdoc, String documento, String nombres, String apellidos, String direccion, String email, String clave) {
        this.id = id;
        this.tipdoc = tipdoc;
        this.documento = documento;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.email = email;
        this.clave = clave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipdoc() {
        return tipdoc;
    }

    public void setTipdoc(String tipdoc) {
        this.tipdoc = tipdoc;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres.toUpperCase();
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos.toUpperCase();
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString(){
        String info = "/*** Información del Usuario ***/\n";
        info+="Documento: (" + this.tipdoc + ") " + this.documento + "\n";
        info+="Nombre Completo: " + this.nombres + " " + this.apellidos + "\n";
        info+="Direccion: " + this.direccion + "\n";
        info+="Email: " + this.email + "\n";
        return info;
    }

    public static boolean validarCampos(String tipDoc, String documento, String nombres, String apellidos, String email, String direccion, String clave)
    {
        return !(tipDoc.isEmpty() || documento.isEmpty() || nombres.isEmpty() || apellidos.isEmpty() || email.isEmpty() || direccion.isEmpty() || clave.isEmpty());
    }
}
