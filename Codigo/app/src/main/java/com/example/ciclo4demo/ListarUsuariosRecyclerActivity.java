package com.example.ciclo4demo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.ciclo4demo.adapters.UsuarioRecyclerAdapter;
import com.example.ciclo4demo.dao.UsuarioDAO;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;

public class ListarUsuariosRecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_usuarios_recycler);

        UsuarioDAO baseDatos = new UsuarioDAO(this);
        ArrayList<Usuario> registros = baseDatos.listar();

        UsuarioRecyclerAdapter adaptadorRecyclerUsuario = new UsuarioRecyclerAdapter(registros);

        RecyclerView recyclerUsuarios = findViewById(R.id.listarUsuariosRecycler_lista);
        recyclerUsuarios.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerUsuarios.setAdapter(adaptadorRecyclerUsuario);

    }
}