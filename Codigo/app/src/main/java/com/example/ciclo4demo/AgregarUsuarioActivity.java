package com.example.ciclo4demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ciclo4demo.dao.UsuarioDAO;
import com.example.ciclo4demo.modelos.Usuario;

public class AgregarUsuarioActivity extends AppCompatActivity {

    private Spinner tipDoc;
    private EditText documento;
    private EditText nombres;
    private EditText apellidos;
    private EditText direccion;
    private EditText email;
    private EditText clave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_usuario);

        tipDoc = findViewById(R.id.buscarUsuario_tipDoc);
        documento = findViewById(R.id.buscarUsuario_documento);
        nombres = findViewById(R.id.buscarUsuario_nombres);
        apellidos = findViewById(R.id.buscarUsuario_apellidos);
        direccion = findViewById(R.id.buscarUsuario_direccion);
        email = findViewById(R.id.buscarUsuario_email);
        clave = findViewById(R.id.buscarUsuario_clave);

        String[] tiposDeDocumentos = {"RC", "CC", "TI", "PA", "DE" };

        ArrayAdapter<String> adaptadorTipDoc = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, tiposDeDocumentos);
        tipDoc.setAdapter(adaptadorTipDoc);

        Button btnGuardar = findViewById(R.id.buscarUsuario_btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _tipdoc = tipDoc.getSelectedItem().toString();
                String _documento = documento.getText().toString();
                String _nombres = nombres.getText().toString();
                String _apellidos = apellidos.getText().toString();
                String _email = email.getText().toString();
                String _direccion = direccion.getText().toString();
                String _clave = clave.getText().toString();

                if(Usuario.validarCampos(_tipdoc, _documento, _nombres, _apellidos, _email, _direccion, _clave))
               {
                   Usuario registro = new Usuario(
                           _tipdoc,
                           _documento,
                           _nombres,
                           _apellidos,
                           _email,
                           _direccion,
                           _clave);
                   UsuarioDAO basedatos = new UsuarioDAO(v.getContext());
                   if(basedatos.insertar(registro)>0)
                       Toast.makeText(v.getContext(), R.string.msj_registro_guardado_OK, Toast.LENGTH_LONG).show();
                   else
                       Toast.makeText(v.getContext(), R.string.msj_registro_guardado_ERROR, Toast.LENGTH_LONG).show();
               }
               else
                   Toast.makeText(v.getContext(), R.string.msj_campos_vacios, Toast.LENGTH_LONG).show();
            }
        });

        Button btnCancelar = findViewById(R.id.buscarUsuario_btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


}