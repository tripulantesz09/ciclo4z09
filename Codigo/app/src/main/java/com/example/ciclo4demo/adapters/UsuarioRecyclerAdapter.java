package com.example.ciclo4demo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ciclo4demo.R;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;

public class UsuarioRecyclerAdapter extends RecyclerView.Adapter<UsuarioRecyclerAdapter.ViewHolderUsuario> {

    private ArrayList<Usuario> usuarios;

    public UsuarioRecyclerAdapter(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @NonNull
    @Override
    public UsuarioRecyclerAdapter.ViewHolderUsuario onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_itemusuario, null, false);

        return new ViewHolderUsuario(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioRecyclerAdapter.ViewHolderUsuario holder, int position) {
        holder.cargarDatos(usuarios.get(position));
    }

    @Override
    public int getItemCount() {
        return usuarios.size();
    }

    public class ViewHolderUsuario extends RecyclerView.ViewHolder {

        private TextView nombreCompleto;
        private TextView direccion;
        private TextView documento;

        public ViewHolderUsuario(@NonNull View itemView) {
            super(itemView);

            nombreCompleto = itemView.findViewById(R.id.itemusuario_nombreCompleto);
            direccion = itemView.findViewById(R.id.itemUsuario_direccion);
            documento = itemView.findViewById(R.id.itemUsuario_documento);
        }

        public void cargarDatos(Usuario registro){
            nombreCompleto.setText(registro.getNombres() + " " + registro.getApellidos());
            direccion.setText(registro.getDireccion());
            documento.setText(registro.getTipdoc() + " - " + registro.getDocumento());
        }
    }
}
