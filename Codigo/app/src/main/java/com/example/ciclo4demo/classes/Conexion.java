package com.example.ciclo4demo.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {
    private static final String baseDatos = "basedatos";
    private static final int version = 1;

    public Conexion(@Nullable Context context) {
        super(context, baseDatos, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE usuarios (\n" +
                "    id        INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "    tipdoc    VARCHAR (2)  NOT NULL,\n" +
                "    documento VARCHAR (11) UNIQUE NOT NULL,\n" +
                "    nombres   VARCHAR (80) NOT NULL,\n" +
                "    apellidos VARCHAR (80) NOT NULL,\n" +
                "    direccion TEXT         NOT NULL,\n" +
                "    email     VARCHAR (50) NOT NULL,\n" +
                "    clave     VARCHAR (50) NOT NULL\n" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
