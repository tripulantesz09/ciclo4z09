package com.example.ciclo4demo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ciclo4demo.dao.UsuarioDAO;
import com.example.ciclo4demo.dialogs.MensajeDialog;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Ciclo4Demo);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnAcceder = findViewById(R.id.login_btnAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se conectan las cajas de texto
                EditText txtUsuario = findViewById(R.id.login_txtUsuario);
                EditText txtClave = findViewById(R.id.login_txtClave);

                //Asignamos los valores de cada caja de texto en las variables usuario y clave
                String usuario = txtUsuario.getText().toString();
                String clave = txtClave.getText().toString();

                //Verificamos que tanto el usuario como la clave no esten vacias
                //si estan vacias mostramos un dialogo de alerta.
                if(!usuario.isEmpty() && !clave.isEmpty()) {
                    Intent intent = new Intent(v.getContext(), InicioActivity.class);
                    intent.putExtra("usuario", usuario);
                    startActivity(intent);
                }
                else
                {
                    AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                    mensaje.setTitle("Campos vacios");
                    mensaje.setMessage(R.string.msj_campos_vacios);
                    mensaje.show();
                }
            }
        });

        ImageButton btnFacebook = findViewById(R.id.login_ibtFacebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.funcionjudicial.gob.ec/www/pdf/preguntasfrecuentescasilleroselectronicos.pdf"));
                startActivity(intent);
            }
        });

        ImageButton btnGoogle = findViewById(R.id.login_ibtGoogle);
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //DialogFragment
                //MensajeDialog fragmento = new MensajeDialog();
                //fragmento.show(getFragmentManager() ,null);

                Toast.makeText(v.getContext(), "Se ha eliminado el registro satisfactoriamente", Toast.LENGTH_SHORT).show();

            }
        });

        ImageButton btnTwitter = findViewById(R.id.login_ibtTwitter);
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario usuario = new Usuario(
                        3,
                        "CC",
                        "25841573",
                        "Lucia",
                        "Fontecha",
                        "Calle 20 # 87 68",
                        "l.fontecha@gmail.com",
                        "789456"
                );

                UsuarioDAO usuarioDao = new UsuarioDAO(v.getContext());
//                System.out.println(usuarioDao.insertar(usuario));

                usuarioDao.eliminar(usuario);

                ArrayList<Usuario> usuariosGuardados = new ArrayList<Usuario>();
                usuariosGuardados = usuarioDao.listar();
            }
        });

    }

    public void ejecutarBoton(View v)
    {
        System.out.println("Hiciste click en boton flotante");
    }
}