package com.example.ciclo4demo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ciclo4demo.R;
import com.example.ciclo4demo.modelos.Usuario;

import java.util.ArrayList;
import java.util.List;

public class UsuarioArrayAdapter extends ArrayAdapter<Usuario> {

    private ArrayList<Usuario> registros;
    private Context contexto;
    private int layoutUtilizado;

    public UsuarioArrayAdapter(@NonNull Context contexto, int layoutUtilizado, ArrayList<Usuario> registros) {
        super(contexto, layoutUtilizado, registros);
        this.registros = registros;
        this.contexto = contexto;
        this.layoutUtilizado = layoutUtilizado;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vista = convertView;

        if(vista==null)
            vista = LayoutInflater.from(contexto).inflate(R.layout.listview_itemusuario, null);

        TextView nombreCompleto = vista.findViewById(R.id.itemusuario_nombreCompleto);
        TextView direccion = vista.findViewById(R.id.itemUsuario_direccion);
        TextView documento = vista.findViewById(R.id.itemUsuario_documento);

        Usuario registroActual = this.registros.get(position);

        nombreCompleto.setText(registroActual.getNombres() + " " + registroActual.getApellidos());
        direccion.setText(registroActual.getDireccion());
        documento.setText(registroActual.getTipdoc() + " - " + registroActual.getDocumento());

        return vista;
    }
}
