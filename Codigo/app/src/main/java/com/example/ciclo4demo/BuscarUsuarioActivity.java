package com.example.ciclo4demo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ciclo4demo.dao.UsuarioDAO;
import com.example.ciclo4demo.modelos.Usuario;

public class BuscarUsuarioActivity extends AppCompatActivity {

    private TextView txvId;
    private EditText txtBuscar;
    private EditText tipdoc;
    private EditText documento;
    private EditText nombres;
    private EditText apellidos;
    private EditText direccion;
    private EditText email;
    private EditText clave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_usuario);

        txtBuscar = findViewById(R.id.buscarUsuario_txtBUscar);
        txvId = findViewById(R.id.buscarUsuario_txvId);
        tipdoc = findViewById(R.id.buscarUsuario_tipDoc);
        documento = findViewById(R.id.buscarUsuario_documento);
        nombres = findViewById(R.id.buscarUsuario_nombres);
        apellidos = findViewById(R.id.buscarUsuario_apellidos);
        direccion = findViewById(R.id.buscarUsuario_direccion);
        email = findViewById(R.id.buscarUsuario_email);
        clave = findViewById(R.id.buscarUsuario_clave);

        Button btnCancelar = findViewById(R.id.buscarUsuario_btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ImageButton ibtBuscar = findViewById(R.id.buscarUsuario_ibtBuscar);
        ibtBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());
                Usuario registro = baseDatos.listarPorDocumento(txtBuscar.getText().toString());
                txvId.setText(String.valueOf(registro.getId()));
                tipdoc.setText(registro.getTipdoc());
                documento.setText(registro.getDocumento());
                nombres.setText(registro.getNombres());
                apellidos.setText(registro.getApellidos());
                direccion.setText(registro.getDireccion());
                email.setText(registro.getEmail());
                clave.setText(registro.getClave());
            }
        });

        Button btnGuardar = findViewById(R.id.buscarUsuario_btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario registro = cargarUsuario();
                if(registro!=null)
                {
                    UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());
                    if(baseDatos.guardarCambios(registro)>0)
                        Toast.makeText(v.getContext(), "Se ha actualizado la informacion", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(v.getContext(), R.string.msj_registro_guardado_ERROR, Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btnEliminar = findViewById(R.id.buscarUsuario_btnEliminar);
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensajeConfirmacion = new AlertDialog.Builder(v.getContext());
                mensajeConfirmacion.setTitle("Confirmación");
                mensajeConfirmacion.setMessage("¿Realmente desea eliminar el registro?");
                mensajeConfirmacion.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Usuario registro = cargarUsuario();
                        if(registro!=null)
                        {
                            UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());
                            if(baseDatos.eliminar(registro)>0)
                                Toast.makeText(v.getContext(), "Se ha eliminado el registro correctamente.", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(v.getContext(), "Ha ocurrido un error al intentar eliminar el registro.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                mensajeConfirmacion.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(v.getContext(), "Se ha cancelado el proceso.", Toast.LENGTH_SHORT).show();
                    }
                });
                mensajeConfirmacion.create();
                mensajeConfirmacion.show();
            }
        });

    }

    private Usuario cargarUsuario()
    {
        Usuario registro = null;
        String id = this.txvId.getText().toString();
        String tipdoc = this.tipdoc.getText().toString();
        String documento = this.documento.getText().toString();
        String nombres = this.nombres.getText().toString();
        String apellidos = this.apellidos.getText().toString();
        String email = this.email.getText().toString();
        String direccion = this.direccion.getText().toString();
        String clave = this.clave.getText().toString();

        double numero = 15.125;
        Double numeroEnvuelto = numero;



        if(!(tipdoc.isEmpty() || documento.isEmpty() || nombres.isEmpty() || apellidos.isEmpty() || email.isEmpty() || direccion.isEmpty() || clave.isEmpty()))
        {
            registro = new Usuario(
                    Integer.parseInt(id),
                    tipdoc,
                    documento,
                    nombres,
                    apellidos,
                    direccion,
                    email,
                    clave);
        }

        return registro;
    }

}