package com.example.ciclo4demo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


public class MensajeDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstance)
    {
        AlertDialog.Builder mensaje = new AlertDialog.Builder(getActivity());
        mensaje.setTitle("Informacion importante");
        mensaje.setMessage("¿Desea eliminar el registro?");
        mensaje.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("Respondiste negativamente");
            }
        });
        mensaje.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("Respondiste afirmativamente");
            }
        });
        return mensaje.create();
    }
}
